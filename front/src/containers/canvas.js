import React, {useEffect, useRef, useState} from 'react';

const Canvas = () => {

    const ws = useRef(null);


    useEffect(() => {
        ws.current = new WebSocket('ws://localhost:8000/canvas');

        ws.current.onmessage = event => {
            const decoded = JSON.parse(event.data);

            if (decoded.type === 'CONNECTED') {
                decoded.drawnPixels.forEach(coordinates => {
                     drawDot(coordinates.x, coordinates.y);
                })
            }

            if (decoded.type === 'NEW_PIXELS') {
                decoded.pixels.forEach(coordinates => {
                    drawDot(coordinates.x, coordinates.y);
                });
            }
        };
    }, []);

    const [state, setState] = useState({

        mouseDown: false,
        pixelsArray: []
    });
    const canvas = useRef(null);

    const canvasMouseMoveHandler = event => {

        if (state.mouseDown) {

            const clientX = event.clientX;
            const clientY = event.clientY;

            setState(prevState => {
                return {
                    ...prevState,
                    pixelsArray: [...prevState.pixelsArray, {
                        x: clientX,
                        y: clientY
                    }]
                };
            });
            drawDot(event.clientX, event.clientY);
        }
    };

    const drawDot = (x, y) => {
        const context = canvas.current.getContext('2d');
        const imageData = context.createImageData(1, 1);
        const d = imageData.data;
        d[0] = 0;
        d[1] = 0;
        d[2] = 0;
        d[3] = 255;
        context.putImageData(imageData, x, y);
    }

    const mouseDownHandler = event => {
        setState({...state, mouseDown: true});
    };

    const mouseUpHandler = event => {

        ws.current.send(JSON.stringify({type: 'NEW_PIXELS', pixels: [...state.pixelsArray]}));
        setState({...state, mouseDown: false, pixelsArray: []});
    };

    return (
        <div>
            <canvas
                ref={canvas}
                style={{border: '1px solid black'}}
                width={800}
                height={600}
                onMouseDown={mouseDownHandler}
                onMouseUp={mouseUpHandler}
                onMouseMove={canvasMouseMoveHandler}
            />
        </div>
    );
};

export default Canvas;