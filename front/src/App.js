import './App.css';
import Canvas from "./containers/canvas";

const App = () => {

  return (
    <div className="App">
        <Canvas />
    </div>
  );
};

export default App;
