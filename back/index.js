const express = require('express');
const cors = require('cors');
const {nanoid} = require('nanoid');
const app = express();

require('express-ws')(app);

const port = 8000;

app.use(cors());

const activeConnections = {};
let drawnPixels = [];

app.ws('/canvas', (ws, req) => {
    const id = nanoid();
    console.log('Client connected!, id = ', id);
    activeConnections[id] = ws;

    ws.send(JSON.stringify({type: 'CONNECTED', drawnPixels}));

    ws.on('message', msg => {
        console.log('Message received!', msg);
        const decoded = JSON.parse(msg);

        if(decoded.type === 'NEW_PIXELS') {
     
            drawnPixels = [...drawnPixels, ...decoded.pixels];
            Object.keys(activeConnections).forEach(key => {
                const connection = activeConnections[key];
                connection.send(JSON.stringify({
                    type: 'NEW_PIXELS',
                    pixels: decoded.pixels,
                }))
            })
        }
    })

    ws.on('close', () => {
        console.log('Client disconnected!, id = ', id);
        delete activeConnections[id];
    });
});

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});